private __gshared {
    enum vgaText = cast(ubyte*)0xFFFF8000000B8000;

    ubyte[2] textPos;

    void putChar(char c, uint x, uint y) {
        vgaText[(x + y*80)*2] = c;
    }
}

void kLog(char c) {
    if (textPos[0] >= 80) {
        //Move onto the next line if the current one is full
        textPos[0] = 0;
        textPos[1] += 1;
    }
    if (textPos[1] >= 25) {
        //Move the log output up if the screen is full
        foreach (i; 0..25) {
            vgaText[i*80*2 .. (i+1)*80*2] = vgaText[(i+1)*80*2 .. (i+2)*80*2];
        }
        textPos[1] = 24;
    }

    switch (c) {
        case '\n': textPos[0] = 0; textPos[1]++        ; break;
        case '\t': textPos[0] += 4                     ; break;
        default  : putChar(c, textPos[0]++, textPos[1]); break;
    }
}

void kLogLn() {
    kLog('\n');
}

void kLog(string text) {
    foreach (i; text) {
        kLog(i);
    }
}

void kLog(const(char)* text) {
    while (*text != '\0') {
        kLog(*(text++));
    }
}

void kLog(bool truth) {
    kLog(truth ? "true" : "false");
}

void kLog(void* ptr) {
    foreach_reverse (i; (cast(ubyte*)&ptr)[0..typeof(ptr).sizeof]) {
        kLog("0123456789ABCDEF"[i >>> 4]);
        kLog("0123456789ABCDEF"[i & 0xF]);
    }
}

void kLog(T)(T value) {
    char[T.max.stringof.length] dig;

    //Check if the number is negative, and if so then make it positive
    bool negative = value < 0;
    if (negative) {
        value *= -1;
    }

    size_t index;
    for (double i = 1; index < dig.length; i *= 10) {
        //Obtain each decimal digit and convert it to a character in a string
        dig[$-(++index)] = "0123456789"[cast(ulong)( (value % (i*10))/i )];
    }
    foreach (i,e; dig) {
        //Print the generated string, ommiting upper digits of 0 (apart from the lowest if the number is 0)
        if (i == dig.length-1 || e != '0') {
            if (negative) {
                kLog('-');
            }
            foreach (f; dig[i..$]) {
                kLog(f);
            }
            return;
        }
    }
}
