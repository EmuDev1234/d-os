module acpi.tables;

import mem;

align(1) struct TableHeader {
  char[4] signature;
  uint    length;
  ubyte   revision;
  ubyte   checksum;
  char[6] OEMID;
  char[8] OEMTableID;
  uint    OEMRevision;
  uint    creatorID;
  uint    creatorRevision;
}

align(1) struct RSDPTable {
    char[8] signature;
    ubyte   checksum;
    char[6] OEMID;
    ubyte   revision;
    uint    rsdtAddress;
}

align(1) struct RSDPTable2 {
    RSDPTable    table;
    uint         length;
    TableHeader* xsdtAddress;
    ubyte        extendedChecksum;
    ubyte[3]     reserved;
}

__gshared {
    RSDPTable* rsdpTable;
    TableHeader* descriptionTable;
}

private bool validChecksum(ubyte[] data) {
    size_t checksum;
    foreach (i; data) {
        checksum += i;
    }
    return !(checksum & 0xFF);
}

TableHeader* findTable(string sig) {
    //Find an ACPI table in the RSDT or XSDT
    assert(sig.length == 4, "Invalid signature for ACPI table search");
    void* seek = cast(void*)descriptionTable + TableHeader.sizeof;
    while (seek < cast(void*)descriptionTable + descriptionTable.length) {
        TableHeader* curTable;
        if (rsdpTable.revision == 0) {
            curTable = cast(TableHeader*)((cast(uint*)seek)[0]);
            seek += 4;
        } else {
            curTable = (cast(TableHeader**)seek)[0];
            seek += 8;
        }

        if ((cast(char*)curTable)[0..4] == sig) {
            return curTable;
        }
    }
    assert(false, "Failed to find requested table in ACPI descriptor table");
}

void acpiInit() {
    //Find the root system descriptor pointer table
    foreach (i; 0..(0xFFFFF-0xE0000)/16) {
        auto ptr = (cast(ubyte*)(0xE0000+(i*16))).toVirtual;
        if (ptr[0..8] == "RSD PTR ") {
            if (ptr[0..20].validChecksum && ((cast(RSDPTable*)ptr).revision == 0 || ptr[0..RSDPTable2.sizeof-3].validChecksum)) {
                rsdpTable = cast(RSDPTable*)ptr; 
            }
        }
    }
    if (!rsdpTable) {
        assert(false, "Failed to find ACPI RSDP table");
    }

    //Save a pointer to the system description table to use
    if (rsdpTable.revision == 0) {
        descriptionTable = (cast(TableHeader*)(rsdpTable.rsdtAddress)).toVirtual;
    } else {
        descriptionTable = (cast(RSDPTable2*)rsdpTable).xsdtAddress.toVirtual;
    }
}
