import klog;

extern(C) {
    void* memset(void* dst, int c, size_t n) {
        foreach (i; 0..n) {
            (cast(ubyte*)dst)[i] = cast(ubyte)c;
        }
        return dst;
    }

    int memcmp(const void* str1, const void* str2, size_t n) {
        foreach (i; 0..n) {
            if ((cast(ubyte*)str1)[i] < (cast(ubyte*)str2)[i]) {
                return -1;
            }
            if ((cast(ubyte*)str1)[i] > (cast(ubyte*)str2)[i]) {
                return 1;
            }
        }
        return 0;
    }

    void _d_array_slice_copy(void* dst, size_t dstlen, void* src, size_t srclen, size_t elementlen) {
        auto length = dstlen*elementlen;
        foreach (i; 0..length) {
            (cast(ubyte*)dst)[i] = (cast(ubyte*)src)[i];
        }
    }

    double fmod(double x, double y) {
        auto div = x/y;
        return y*(div - (cast(size_t)div));
    }

    void __assert(const char* msg, const char* file, int line) {
        kLog("Assertion failed: ");
        kLog(msg);
        kLog(" at ");
        kLog(file);
        kLog('@');
        kLog(line);
        kLog('\n');
        while (true) asm {
            cli;
            hlt;
        }
    }
}
