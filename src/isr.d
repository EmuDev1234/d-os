import klog;
import mem;

alias ISR = void*;

void halt() {
    while (true) asm {
        cli;
        hlt;
    }
}

void unhandledInt(string msg) {
    kLog("An unhandled interrupt occured: \n\t");
    kLog(msg);
    kLog('\n');
    halt;
}

void gpf()  { unhandledInt("General protection fault"); }
void stub() { unhandledInt("Stub handler");             }

void pageFault() { 
    asm {
        naked;
        push RAX;
        push RCX;
        push RDX;
        push RSI;
        push RDI;
        push R8;
        push R9;
        push R10;
        push R11;
        push R12;
        push R13;
        push R14;
        push R15;

        mov RDX, CR3;
        mov RSI, PageEntryFlags.Present | PageEntryFlags.Writable;
        mov RDI, CR2;
        call mapKernelPage;

        pop R15;
        pop R14;
        pop R13;
        pop R12;
        pop R11;
        pop R10;
        pop R9;
        pop R8;
        pop RDI;
        pop RSI;
        pop RDX;
        pop RCX;
        pop RAX;
        add RSP, 8;
        iretq;
    }
}
