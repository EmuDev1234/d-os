import klog;
import mem;
import interrupt;
import isr;
import acpi.tables;

extern(C) void main() {
    setupIDT;
    setISR(13, &gpf);
    setISR(14, &pageFault);

    //Print information about the kernel memory map
    mmInit;
    size_t memSize;
    foreach (ref i; memMap) {
        //Print information about each memory region
        kLog(cast(void*)i.base);
        kLog('-');
        kLog(cast(void*)i.base + i.len);
        kLog(i.usable ? " (Usable)\n" : " (Unusable)\n");
        if (i.usable) {
            memSize += i.len;
        }
    }
    kLog(memSize/(1024^^2));
    kLog("M RAM detected\n");

    acpiInit;
    kLog("Vendor: "); 
    kLog(cast(string)(rsdpTable.OEMID[0..$]));
    kLogLn;
    auto pcieTable = findTable("MCFG");
    kLog("Found PCIe configuration ACPI table! ");
    kLog(pcieTable);
}
