.section .multiboot
    .align 8
    //--Magic number
    .int 0xE85250D6 
    //--32-bit protected mode IA32
    .int 0 
    //--Length of header
    .int 24
    //--Checksum value
    .int -(0xE85250D6+0+24)
    //--Tag end (type 0 and size 8)
    .int 0 
    .int 8

.section data
    .globl multiboot2_info
    multiboot2_info:
        .skip 8

.section .data
    //--Initial page map
        .align 4096
    pml1:
        .skip 512*8
        .align 4096
    pml2:
        .quad (pml1-0xFFFF800000000000) + 0x3
        .skip (512-1)*8
        .align 4096
    pml3:
        .quad (pml2-0xFFFF800000000000) + 0x3
        .skip (512-1)*8  
        .align 4096

    pml1_window:
        .skip 511*8
    pml1_window_entry:
        .globl pml1_window_entry
        .skip 8
    pml2_window:
        .skip (512-1)*8
        .quad (pml1_window-0xFFFF800000000000) + 0x3
    pml3_window:
        .skip (512-1)*8
        .quad (pml2_window-0xFFFF800000000000) + 0x3

    pml4:  
        .quad (pml3-0xFFFF800000000000) + 0x3
        .skip (256-1)*8
        .quad (pml3-0xffff800000000000) + 0x3
        .skip (256-2)*8
        .quad (pml3_window-0xFFFF800000000000) + 0x3

    //--Global descriptor table
    gdt_address:
        .short 8*4 - 1
        .quad gdt_base
    gdt_base:
        .skip 8
    gdt_code:
        .short 0xFFFF
        .short 0
        .byte 0
        .byte 0b10011010
        .byte 0b10101111
        .byte 0
    gdt_data:
        .short 0xFFFF
        .short 0
        .byte 0
        .byte 0b10010010
        .byte 0b10001111
        .byte 0

.section .bss
    .skip 64*1024
    stack_top:

.section .text.boot
    .code32
    .globl boot
    boot:
        //Save the address to the multiboot2 information structure
        mov %ebx, multiboot2_info
        movl $0xFFFF8000, multiboot2_info+4

        //Initialise the page map level 1 structures to use temporarily
        xor %ecx, %ecx
        mov $(pml1-0xFFFF800000000000), %edi
        initloop:
            mov %ecx, %eax
            mov $4096, %edx
            mul %edx
            add $0x3, %eax
            mov %eax, (%edi)
            add $8, %edi
            inc %ecx
            cmp $512, %ecx
            jne initloop

        //Setup SSE, paging, and enable long mode
        mov %cr0, %edx
        or $0x2, %edx
        and $0xFFFFFFFD, %edx
        mov %edx, %cr0

        mov %cr4, %edx
        or $0x620, %edx
        mov %edx, %cr4
        mov $(pml4-0xFFFF800000000000), %edx
        mov %edx, %cr3

        mov $0xc0000080, %ecx
        rdmsr
        or $0x100, %eax
        wrmsr
        mov %cr0, %edx
        or $0x80000000, %edx
        mov %edx, %cr0

        //Enter proper long mode
        lgdt gdt_address-0xFFFF800000000000
        jmp $8, $boot64

    .code64
    .globl main
    boot64:
        movabs $stack_top, %rsp
        mov $16, %ax
        mov %ax, %ds
        mov %ax, %ss

        //Leave bootstrap and enter main kernel
        movabs $enter_main, %rax
        jmp %rax

.section .text
    enter_main:
        movabs $gdt_address, %rax
        lgdt (%rax)
        movabs $pml4, %rax
        movq $0, %rax

        call main
        cli
            loop:
                hlt
                jmp loop
        
