import isr;

align(1) struct Gate {
    ushort isrAddress0;
    ushort gdtSelectorLower;
    ubyte  istIndex;
    ubyte  type;
    ushort isrAddress1;
    uint   isrAddress2;
    uint   empty;
}

private __gshared Gate[256] idt;

void setISR(size_t interrupt, ISR isr) {
    with (idt[interrupt]) {
        isrAddress0      = cast(ushort)isr;
        gdtSelectorLower = 8;
        istIndex         = 0;
        type             = interrupt < 32 ? 0x8F : 0x8E;
        isrAddress1      = cast(ushort)(cast(size_t)isr >> 16);
        isrAddress2      = cast(uint)(cast(size_t)isr >> 32);
        empty            = 0;
    }
}

void setupIDT() {
    //Initialise the IDT and then load it
    foreach (i; 0..idt.length) {
        setISR(i, &stub);
    }

    size_t[2] idtr = [(cast(size_t)&idt << 16) | typeof(idt).sizeof-1, 0xFFFF];
    asm {
        lidt idtr;
    }
}
