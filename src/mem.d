/***************** Types and constants *****************/
alias AddrPlaceholder = immutable(void)*;

alias PageEntry = ulong;
alias PageTable = PageEntry[512];
alias Page      = ubyte[4096];
enum PageEntryFlags {
    None     = 0x000,
    Present  = 0x001,
    Writable = 0x002
}

struct MemoryRegion {
    void* base;
    size_t len;
    bool usable;
}

/***************** Internal variables *****************/
private __gshared {
    extern pragma(mangle, "kernel_base"    ) AddrPlaceholder kernelBase;
    extern pragma(mangle, "kernel_end"     ) AddrPlaceholder kernelEnd;
    extern pragma(mangle, "multiboot2_info") void* multiboot2Info;

    extern pragma(mangle, "pml1_window_entry") PageEntry windowPageEntry;
    extern pragma(mangle, "window_page") Page windowPage;

    MemoryRegion[32] memRegions;
    ubyte[] allocMap;
}
__gshared const(MemoryRegion)[] memMap;

/***************** Functions *****************/
private void fillMemoryMap() {
    //Find the memory map structures using the multiboot2 information structure
    void* curTag = multiboot2Info+8;
    void* endAddress = multiboot2Info + (cast(uint*)multiboot2Info)[0];

    while (curTag < endAddress)  {
        //Iterate over the tags until a memory map tag is found or the information structure is exhausted
        if (*(cast(uint*)curTag) != 6) {
            //Find the next tag if this one is not a memory map tag
            curTag += (cast(uint*)curTag)[1];
            if (cast(size_t)curTag % 8) {
                //Round up for 8 byte alignment
                curTag += 8 - (cast(size_t)curTag%8);
            }
        } else {
            //Fill an array of MemoryRegion values describing the memory regions available
            size_t entrySize = (cast(uint*)curTag)[2];
            size_t numRegions = ((cast(uint*)curTag)[1] - 16) / entrySize;
            void* curEntry = curTag+16;
            endAddress = curTag+((cast(uint*)curTag)[1]);

            uint entryNum;
            while (curEntry <= endAddress) {
                if (entryNum < numRegions) {
                    memRegions[entryNum] = MemoryRegion(*(cast(void**)curEntry), *(cast(ulong*)(curEntry+8)), *(cast(uint*)(curEntry+16)) == 1);
                    curEntry += entrySize;
                    ++entryNum;
                } else {
                    memMap = memRegions[0..numRegions];
                    return;
                }
            }
            assert(false, "Seeking past end of memory map");
        }
    }
    assert(false, "Failed to find memory map");
}

private void allocMemAllocMap() {
    //Find the size of all available address space regions combined
    size_t memSize;
    foreach (ref i; memMap) {
        memSize += i.len;
    }

    //Allocate a memory map bitmap beginning at the end of the kernel's sections
    size_t mapSize = (memSize/(4096*8))+1;
    allocMap = (cast(ubyte*)&kernelEnd)[ 0 .. mapSize ];

    //Mark the address region used by the kernel as taken
    allocMap[] = 0;
    size_t mapEnd = cast(size_t)&kernelEnd-cast(size_t)&kernelBase + mapSize;
    fillAllocMap(null, cast(void*)mapEnd, true);

    //Mark other unusable address regions as taken
    foreach (ref i; memMap) {
        if (!i.usable) {
            auto alignBase = cast(void*)( ((cast(size_t)i.base)/4096)*4096 );
            fillAllocMap(alignBase, alignBase + ((i.len/4096) + 1)*4096, true);
        }
    }
}

private void setWindowPage(Page* physAddr, PageEntryFlags flags = PageEntryFlags.Present | PageEntryFlags.Writable) {
    //Map a page of the physical address space to the virtual window page, correcting the address as needed
    auto toSet = cast(PageEntry)((cast(size_t)physAddr/4096)*4096 + flags);
    if (toSet != windowPageEntry) {
        windowPageEntry = toSet;
        asm {
            invlpg windowPage;
        }
    }
}

private void setAllocMap(void* physAddr, bool alloc) {
    //Set or clear a single flag in the allocation map
    size_t index = cast(size_t)physAddr / (4096*8);
    ubyte bit = 0x80 >>> ((cast(size_t)physAddr % (4096*8))/4096);
    if (alloc) {
        allocMap[index] |= bit;
    } else {
        allocMap[index] &= bit;
    }
}

private void fillAllocMap(void* beginAddr, void* endAddr, bool alloc) {
    //Round up each address if it is within a page, not at the start
    auto begin = (cast(size_t)beginAddr)/4096;
    if (cast(size_t)beginAddr % 4096) {
        begin += 1;
    }
    begin *= 4096;

    auto end = (cast(size_t)endAddr)/4096;
    if (cast(size_t)endAddr % 4096) {
        end += 1;
    }
    end *= 4096;

    //Calculate number of pages, as well as the indexes into the allocation map for the beginning and end addresses
    auto pages = (end-begin) / 4096;
    auto beginMapIndex = begin/(4096*8);
    auto endMapIndex = end/(4096*8);

    //Fill each flag in the start byte
    auto beginStartFlag = ((begin%(4096*8))/4096);
    auto endFinalFlag = ((end%(4096*8))/4096);
    foreach (i; beginStartFlag .. beginMapIndex == endMapIndex ? endFinalFlag : 8) {
        if (alloc) {
            allocMap[beginMapIndex] |= 0x80 >>> i;
        } else {
            allocMap[beginMapIndex] &= 0x80 >>> i;
        }
    }
    if (beginMapIndex == endMapIndex) {
        return;
    }

    //Fill all bytes in the middle
    pages -= 7-beginStartFlag;
    allocMap[beginMapIndex+1 .. beginMapIndex+1+(pages/8)] = alloc ? 0xFF : 0x00;

    //Fill each flag in the end byte
    foreach (i; 0..endFinalFlag) {
        if (alloc) {
            allocMap[endMapIndex] |= 0x80 >>> i;
        } else {
            allocMap[endMapIndex] &= 0x80 >>> i;
        }
    }
}

T* toPhysical(T)(T* ptr) {
    return cast(T*)(cast(size_t)ptr - cast(size_t)&kernelBase);
}

T* toVirtual(T)(T* ptr) {
    return cast(T*)(cast(size_t)ptr + cast(size_t)&kernelBase);
}

Page* allocPage() {
    //Return a page in the physical address space, marking it as allocated
    foreach (i, ref e; allocMap[0 .. $]) {
        if (e != 0xFF) {
            //Find the lowest page that is specified as usable in this byte
            foreach (f; 0..8) {
                if (!((e >> 7-f) & 0x1)) {
                    //Calculate the segments position and set the corresponding flag to indicate usage
                    size_t addr = i*4096*8 + f*4096;
                    setAllocMap(cast(void*)addr, true);
                    
                    //Clear the page before returning it's physical address
                    auto toReturn = cast(Page*)addr;
                    setWindowPage(toReturn);
                    windowPage = 0;
                    return toReturn;
                }
            }
        }
    }
    assert(false, "Failed to allocate page");
}

Page* allocPage(PageTable* table, PageEntryFlags flags, uint index) {
    //Allocate a page and fill in the page table to reference this new page at the specified index
    auto newPage = allocPage;
    setWindowPage(cast(Page*)table);
    (cast(PageEntry*)&windowPage)[index] = cast(PageEntry)(cast(void*)newPage + flags);
    return newPage;
}

void mapKernelPage(PageTable* root, PageEntryFlags flags, void* addr) {
    //Map a physical page to kernel space by filling in the page map whose pointer is passed
    assert(addr >= &kernelBase, "Invalid address for kernel page to map");
    auto toSet = addr.toPhysical;
    addr = cast(void*)(cast(size_t)addr & 0xFFFFFFFFFFFF);
    uint level = 3;

    foreach (i; 0..4) {
        auto index = cast(size_t)(addr) / (4096*(ulong(512)^^level));
        assert(index < 512, "Index into page table is greater than length");
        addr -= (ulong(512)^^level)*4096*index;
        setWindowPage(cast(Page*)root);

        if (level-- > 0) {
            auto oldRoot = root;
            root = cast(PageTable*)( (cast(PageEntry*)&windowPage)[index] & ~0xFFF );
            if (!root) {
                //If there is no page entry there then allocate a new page table
                root = cast(PageTable*)allocPage;
                setWindowPage(cast(Page*)oldRoot);
                (cast(PageEntry*)&windowPage)[index] = cast(PageEntry)root + flags;
            }
        } else {
            (cast(PageEntry*)&windowPage)[index] = (cast(PageEntry)toSet & ~0xFFF) + flags;
        }
    }
}

void freePage(void* page) {
    //Free a page in the physical address space, marking it as deallocated
    setAllocMap(page, false);
}

PageTable* createPageMap() {
    //Create a new page map, ensuring that the kernel is mapped in the higher half
    auto pml4 = cast(PageTable*)allocPage;
    PageTable* curMap;
    asm {
        mov RAX, CR3;
        mov curMap, RAX;
    }
    foreach (i; 256..512) {
        (*pml4)[i] = (*curMap)[i];
    }    

    return pml4;
}

void freePageTable(PageTable* root, uint level = 0) {
    //Free a page table, and all page tables or pages referenced by it
    assert(level <= 3, "Invalid page map level when freeing page table");
    foreach (i; (*root.toVirtual)[0..512]) {
        if (level < 3 && i) {
            freePageTable(cast(PageTable*)(i & ~0xFFF), level+1);
        }
        freePage(cast(void*)i);
    }
    freePage(cast(void*)root);
}

void loadPageMap(PageTable* table) {
    asm {
        mov RAX, table;
        mov CR3, RAX;
    }
}

void mmInit() {
    fillMemoryMap;
    allocMemAllocMap;
}
