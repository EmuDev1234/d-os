#!/usr/bin/rdmd
import std.process,
       std.stdio,
       std.file;

void exec(string cmd) {
    writeln(cmd.executeShell[1]);
}

void main() {
    //Build all object files
    chdir("src");
    if (!"../build".exists) {
        mkdir("../build");
    }
    exec("as boot.s -c -nostdlib -o ../build/boot.o");
    exec("ldc2 $(find . | grep \"**\\.d\") -c --code-model=large -O3 --betterC --disable-red-zone -mtriple=elf-x86_64 -od ../build/");
    chdir("../build");
    mkdirRecurse("boot/grub");
    exec("ld.lld **.o -T ../src/cfg/link.ld");

    //Create a GRUB iso
    copy("../src/cfg/grub.cfg", "boot/grub/grub.cfg");
    std.file.remove("dos.iso");
    exec("grub-mkrescue . -o dos.iso");
}

